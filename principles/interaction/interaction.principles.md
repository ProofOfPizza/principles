## Name: Interactions
## Parent: [Main](../main.principles.md)
## Children:
- ### [People](./people/people.principles.md)
## Principles:
- be firm on principles and soft on people
- respect (and do not interrupt or waste people's time)
- listen
- disagree or actively support
