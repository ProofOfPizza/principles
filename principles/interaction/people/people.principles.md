## Name: People principles
## Parent: [Interaction](../../interaction/interaction.principles.md)
## Children:
- ### [Hiring](./hiring/hiring.people.principles.md)
- ### [Firing](./firing/firing.people.principles.md)
- ### [Promoting](./promoting/promoting.people.principles.md)
## Principles:
