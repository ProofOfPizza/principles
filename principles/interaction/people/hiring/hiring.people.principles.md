## Name: Hiring People
## Parent: [People](../people.principles.md)
## Children:
## Principles:
- Look for people that heve the following:
  - values:
    - their values align with our values
  - abilities:
    - are smart
    - are proactive
    - are creative
    - show integrity
    - are eager to learn
    - communicate clearly
    - are autonomous
    - add something unique to the whole
    - are realists (about others and external life as much as about themselves)
    - know when and how to get help
    - are bold
    - own the results of their choices, be they gains or pains
    - are teachers
    - have a factor X
  - skills:
    - [directly relevant](../experience/experience.principles.md)
    - [indirectly relevant](../experience/experience.principles.md)
  - ambitions:
    - work related ambitions align with required ecpertise
- Evaluate these principles on 1-4 scale: 1 Not at all, 2 a bit, 3 quite, 4 very
- Positive signal is any x >= 3, negative signal is any x <=2, discuss 2<x<3
- The right people are more valuable (in secondary and onwards results) than right experience!

