## Name: Firing People
## Parent: [People](../people.principles.md)
## Principles:
- Firing the right people is just important as hiring the right people
- Fire people as quickly as possible if and only if necessary
- Fire people who:
  - are unbelievable agents (in principle evaluation)
  - do not fulfil commitments
  - lack transparency
  - do not prioritize the whole (team) over the individual

