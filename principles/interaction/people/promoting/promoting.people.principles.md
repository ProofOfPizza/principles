## Name: Promoting People
## Parent: [People](../people.principles.md)
## Principles:
- Promoting people quickly if they:
  - are believable agents (in evaluating principles)
  - gain [direct relevant experience](../experience/experience.principles.md)
  - gain [indirectly relevant experience](../experience/experience.principles.md)
  - does the current situation put the person in a different experience scale ?
  - expand: new areas of expertise / experience ?
  - actively contribute to the whole
  -

