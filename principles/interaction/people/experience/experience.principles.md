## Name: Experience
## Parent: [People](../people.principles.md)
## Children:
- ### [programmer](./programmer/programmer.experience.principles.md)
## Principles:
- How does the current [experience](../experience/experience.principles.md) evaluate ?
- For each category (direct and indirectly relevant experiences) average score evaluates as:
  - 1 <= x < 1.5 : starter
  - 1.5 <= x < 2 : basic
  - 1.5 <x x < 2.5 : intermediate
  - 2.5 <= x < 3 : experienced
  - 3 <= 3.5 < 4 : expert
