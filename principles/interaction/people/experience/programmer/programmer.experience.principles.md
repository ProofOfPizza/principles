## Name: Programmer Experience
## Parent: [People](../people.principles.md)
## Children:
## Principles:
- Look for people that have direct experience with:
  - java
  - javascript
  - typescript
  - spring
  - react
  - angular
  - microservices
  - decoupled architectures (message busses)
  - cloud
  - AWS
  - CI/CD
  - IAC
  - Observability
  - Git
  - Pair programming
  - Devops
  - Trunk based development
  - Other technical experience
- Evaluate these principles on 1-4 scale: 1 None, 2 can use with help or after finding out, 3 have used , 4 very
- Look for people that have other/indirectly relevant experience with:
  - software development projects
  - agile
  - teaching
  - leading teams
  - building teams
  - entrepreneurship
  - Other indirectly relevant experience
- Evaluate these principles on 1-4 scale: 1 None, 2 some experience possibly non professional, 3 professional experience , 4 autonomously / can share experience with others
- Positive signal is any x >= 3, negative signal is any x <=2, discuss 2<x<3

