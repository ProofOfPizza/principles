## Name: Main
## Parent: None
## children: [interactions](./interaction/interaction.principles.md)
## Principles:
- Reality / nature optimizes for the whole, sometimes at the expense of the parts. Good decisions align with that principle, bad decisions go against it.
- Any principle should exist if and only if it promotes getting to the best possible decision in a reproducible way.
- Anyone can propose (C)reate (U)pdate and (D)elete operations for any principle.
- Any decision should be made based on the merits of the idea and the principles. (As opposed to things like seniority, race, gender etc)
- Evaluating and weighing principles should be done considering:
  - second, third and more order consequences
  - believability of evaluating agents
- Any principles should lead to decisions that are mission aligned
