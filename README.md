# principles

This project holds the guiding principles for Adabtive. It is a tree structure starting with the overarching goals, and branching out into more detailed sets of principles used for decision making.

The goal of this approach is to create and maintain a system that optimizes decision making: Getting as to the best decisions in the shortest time and have it.

Any set of principles has a type, in pseudo code because all of this are just markdown files:

name: Name,

principles: Principle[]

parent: Name | None

children: Name[]


